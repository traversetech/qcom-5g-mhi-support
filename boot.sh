#!/bin/sh
set +e

CURDIR=$(dirname "$0")
PCIPATH="/sys/bus/pci/devices/0000:01:00.0"

is_mhi_driver_attached() {
	[ ! -d "${PCIPATH}/driver" ] && return 1
	ATTACHED_DRIVER=$(readlink -f "${PCIPATH}/driver")
	[ "${ATTACHED_DRIVER}" = "/sys/bus/pci/drivers/mhi-pci-generic" ] && return 0
	return 1
}

if [ -d "${PCIPATH}" ]; then
	if is_mhi_driver_attached; then
		echo "ERROR: Modem device appears to be online and operational alredy"
		echo "Run ./poweroff.sh to remove it"
		exit 1
	else
		echo "Card enumerated but not attached, power cycling it first"
		"$CURDIR"/poweroff.sh
		sleep 2 # let card finish power off
	fi
fi

POWER_OFF_GPIO=377
RESET_GPIO=376
if [ -d "/sys/class/gpio/gpiochip640" ]; then
	POWER_OFF_GPIO=649
	RESET_GPIO=648
fi

# FULL CARD POWER OFF
if [ ! -d "/sys/class/gpio/gpio${POWER_OFF_GPIO}" ]; then
	echo "${POWER_OFF_GPIO}"> /sys/class/gpio/export
	echo out > "/sys/class/gpio/gpio${POWER_OFF_GPIO}/direction"
fi
echo 0 > "/sys/class/gpio/gpio${POWER_OFF_GPIO}/value"

# RESET
if [ ! -d "/sys/class/gpio/gpio${RESET_GPIO}" ]; then
	echo "${RESET_GPIO}" > /sys/class/gpio/export
	echo out > "/sys/class/gpio/gpio${RESET_GPIO}/direction"
fi

echo 1 > "/sys/class/gpio/gpio${RESET_GPIO}/value"
sleep 1
echo 0 > "/sys/class/gpio/gpio${RESET_GPIO}/value"

for i in $(seq 0 10); do
	[ -d "${PCIPATH}" ] && break
	echo 1 > /sys/bus/pci/rescan
	sleep 2
done

if [ ! -d "${PCIPATH}" ]; then
	echo "ERROR: No PCIe device found"
	exit 1
fi

echo on > "${PCIPATH}/power/control"
modprobe mhi_pci_generic

for i in $(seq 0 10); do
	is_mhi_driver_attached && break
	sleep 1
done

is_mhi_driver_attached || {
	echo "ERROR: MHI has not attached to modem"
	exit 1
}

# Loading MHI driver resets power control status :(
echo on > "${PCIPATH}/power/control"
echo "Modem ready"
