#!/bin/sh
PCIBUSLOC="0000:01:00.0"

if [ -d "/sys/bus/pci/drivers/mhi-pci-generic/${PCIBUSLOC}" ]; then
	echo "${PCIBUSLOC}" > /sys/bus/pci/drivers/mhi-pci-generic/unbind
fi

if [ -d "/sys/bus/pci/devices/${PCIBUSLOC}" ]; then
	echo 1 > "/sys/bus/pci/devices/${PCIBUSLOC}/remove"
fi

GPIO=377
if [ -d "/sys/class/gpio/gpiochip640" ]; then
		GPIO=649
fi

if [ ! -d "/sys/class/gpio/gpio${GPIO}" ]; then
	echo "${GPIO}" > /sys/class/gpio/export
	echo out > "/sys/class/gpio/gpio${GPIO}/direction"
fi

echo 1 > "/sys/class/gpio/gpio${GPIO}/value"

