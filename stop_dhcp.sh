#!/bin/sh
PDH=$(grep -Po 'Packet data handle\: .(\K[[:digit:]]+)' /tmp/conncid.txt)
CID=$(grep -Po 'CID\: .(\K[[:digit:]]+)' /tmp/conncid.txt)
echo "${CID} ${PDH}"
qmicli -d /dev/wwan0qmi0 --client-cid="${CID}" --wds-stop-network="${PDH}"
