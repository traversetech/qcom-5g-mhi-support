# Setup scripts for Qualcomm PCIe / MHI bus modems on Ten64

New 5G modems using Qualcomm chipsets such as the SDX55
support PCIe modes using Qualcomm's MHI interface.

These modems behave slightly differently in USB mode,
and since they are very new, kernel oops/panics can
happen in some edge cases.

We have found these scripts are the most optimal way to
bring them up.

## Tested modules

* Sierra Wireless EM9191
* Telit FN980m Rev 1 (Rev2 as yet untested)
* Quectel RM500Q-GL
* SIMCOM SIM8200EA

## Power up process

* It is best to hold the modem in power off until you
are ready to use it. Some modem models will time out
(not boot) if a driver does not take control them and can
only be recovered by restarting them via control lines.

* We suggest adding `mhi_pci_generic` to the module blacklist
so initrd's do not modprobe this module unncessarily

```
echo "blacklist mhi_pci_generic" >> /etc/modprobe.d/blacklist.conf
depmod -a
update-initramfs -k $(uname -r) -c
```

(Note: this will also affect other MHI cards such as ath11k WiFi)

* PCI Express power management (ASPM) appears to cause
  issues with MHI hardware that goes to idle mode.

  We suggest adding `pcie_aspm=off` to the kernel command line.

  On Debian/Ubuntu, this can be done by editing `GRUB_CMDLINE_LINUX_DEFAULT`
  in `/etc/default/grub` and then running `update-grub`

  Further, if the card is enumerated and ready at boot, it is recommended
  to power it off (`./poweroff.sh`) and restart it with `./boot.sh` to 
  ensure the power management options are disabled completely.

## Scripts

* `./boot.sh`

Powers up the modem and attaches the MHI driver

* `./start_dhcp.sh`

  Starts a network instance using mhi_net and DHCP.
  (Note: this is not the fastest way to use the module, but
  most convenient for testing)

  Note: Please edit your APN settings before using.

  `udhcpc` is required, as this program supports
  sending out DHCP packets on non-Layer 2 interfaces.
  
  Alternatively, you may use a command like
  `qmicli -d /dev/wwan0qmi0 -p --wds-get-current-settings` to retrieve
  the current connection settings and add them to the `mhi_hwip0` interface
  manually.

  TODO: Add support for IPv6 only APNs (in the meantime,
  these have to be setup manually using the details from qmicli above).

* `./stop_dhcp.sh`
Stops the current cellular connection

* `./poweroff.sh`
Detaches the MHI driver and issues power off command to the modem

## TODO:

* Create C version using the new GPIO / char device API, as sysfs gpio is deprecated :(

* Investigate possible rfkill integration

